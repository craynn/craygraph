from ..meta import get_incoming

__all__ = [
  'get_nodes',
  'get_inputs_nodes',
  'select_subgraph',

  'map_graph'
]

def get_nodes(nodes, incoming=get_incoming):
  from ..evaluate.common import propagate
  return list(
    propagate(
      f=lambda node, *args: node,
      nodes=nodes,
      substitutes=dict(),
      incoming=incoming
    ).values()
  )

def map_graph(f, nodes):
  from ..evaluate.common import propagate

  if not isinstance(nodes, (list, tuple)):
    nodes = [nodes]

  return list(
    propagate(
      f=lambda node, *args: f(node),
      nodes=nodes,
      substitutes=dict()
    ).values()
  )

def get_inputs_nodes(nodes, incoming=get_incoming):
  return [
    node
    for node in get_nodes(nodes)
    if len(incoming(node)) == 0
  ]

def select_subgraph(predicate, nodes, single=False, incoming=get_incoming):
  results = []

  if not isinstance(nodes, (list, tuple)):
    stack = [nodes]
  else:
    stack = list(nodes)

  visited = set()

  while len(stack) > 0:
    current = stack.pop()
    if current in visited:
      continue

    if predicate(current):
      if single:
        return current
      else:
        results.append(current)

    incs = incoming(current)
    stack.extend(incs)

  if single:
    return None
  else:
    return results