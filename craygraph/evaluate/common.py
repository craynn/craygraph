from ..meta import get_incoming

__all__ = [
  'propagate',
  'reduce_graph',
  'graph_reducer'
]

def propagate(f, nodes, substitutes=None, incoming=get_incoming):
  """
  For each node in the graph computes:

      r(node) = f(node, [r(x) for x in incoming(node)])

  `graph` is defined as `nodes` and all their dependencies.
  This function caches results `r(node)`, thus, `f`.

  NB: if substitutes are specified not all nodes might be evaluated, also not all nodes might
    be present in the result.

  :param f: operator to propagate, a function of type `(Node, List[A]) -> A`;
  :param nodes: a list of nodes, output nodes of the lang `f` is to be propagated through;
  :param substitutes: a dictionary `Node -> A`, overrides results of `r(node)`,
    None is the same as an emtpy dictionary;
  :param incoming: operator `Node -> List[Node]`, returns list of incoming nodes (dependencies);
  :return: dictionary `Node -> A`.
  """
  if substitutes is not None:
    known_results = substitutes.copy()
  else:
    known_results = dict()

  stack = list()
  stack.extend(nodes)

  while len(stack) > 0:
    current_node = stack.pop()

    if current_node in known_results:
      continue

    incoming_nodes = incoming(current_node)

    unknown_dependencies = [
      nodes
      for nodes in incoming_nodes
      if nodes not in known_results
    ]

    if len(unknown_dependencies) == 0:
      args = tuple(known_results[node] for node in incoming_nodes)
      known_results[current_node] = f(current_node, args)

    else:
      stack.append(current_node)
      stack.extend(unknown_dependencies)

  return known_results

def reduce_graph(f, nodes, substitutes=None, incoming=get_incoming):
  """
    The same as `propagate` but returns results only for `nodes`:

        r(node) = f(node, [r(x) for x in incoming(node)])

    :param f: operator to propagate, a function of type `(Node, List[A]) -> A`;
    :param nodes: a list of nodes or a node --- output nodes of the lang `f` is to be propagated through;
    :param substitutes: a dictionary `Node -> A`, overrides results of r(node),
      None is the same as an emtpy dictionary;
    :param incoming: operator `Node -> List[Node]`, returns list of incoming nodes (dependencies);
    :return: list of results (if `nodes` is a collection of nodes), or
      just result for the `nodes` (in case `nodes` is a single node)
    """

  if isinstance(nodes, (tuple, list)):
    result = propagate(f, nodes, substitutes=substitutes, incoming=incoming)
    return tuple(
      result[node]
      for node in nodes
    )

  else:
    result = propagate(f, (nodes, ), substitutes=substitutes, incoming=incoming)
    return result[nodes]

def graph_reducer(operator, strict=False):
  """
    Wraps operator into `propagate`-operator.

  :param operator: `Node` -> function
  :param strict: if `False` use `apply_with_kwargs` wrapper on `operator` which filters key word arguments before passing
    them into the propagated function; otherwise, passes `**kwargs` directly to the propagated function.
  :return: a getter, function `(list of nodes, substitution dictionary=None, **kwargs) -> value`
    that computes the operator output for `nodes`.
  """
  def getter(nodes_or_node, substitutes=None, **kwargs):
    from ..meta import apply_with_kwargs, Node

    if not isinstance(nodes_or_node, (list, tuple)):
      nodes = [nodes_or_node]
    else:
      nodes = nodes_or_node

    if substitutes is None:
      substitutes = dict()

    if strict:
      wrapped_operator = lambda node, args: operator(node)(*args, **kwargs)
    else:
      wrapped_operator = lambda node, args: apply_with_kwargs(operator(node), *args, **kwargs)

    results = propagate(wrapped_operator, nodes, substitutes)

    if isinstance(nodes_or_node, Node):
      return results[nodes_or_node]
    else:
      return [results[node] for node in nodes_or_node]

  return getter
