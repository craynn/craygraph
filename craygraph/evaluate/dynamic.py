from ..meta import get_incoming

__all__ = [
  'propagate',
  'reduce_graph'
]

def propagate(f, nodes, substitutes=None, incoming=None):
  """
  Similar to `propagate` but allows to dynamically compute incoming nodes and pass results of `incoming(node)` to `f`.
  For each node in the lang computes:

      r(node) = f(node, [r(x) for x in incoming_nodes], q(node))
      where:
        incoming_nodes, intermediate_result = incoming(node)

  Graph is defined as `nodes` and all their dependencies.
  This function caches results `r(node)`, thus, `f`.

  Note, that unlike `common.propagate`, operator `f` receives 3 arguments:
  - node;
  - incoming nodes (dependencies) that *need to be computed*;
  - value returned by `incoming`.

  This is useful for implementing cached dataflows, when dependencies depend on results of cache retrieval:

      def incoming(node):
        try:
          return list(), load_cache(node)
        except:
          return node.incoming, None

      def operator(node, inputs, cached):
        if cached is not None:
          return cached
        else:
          <perform computations>

  NB: if substitutes are specified not all nodes might be evaluated, also not all nodes might
    be present in the result.

  :param f: operator to propagate, a function of type `(Node, List[A], B) -> A`;
  :param nodes: a list of nodes, output nodes of the lang `f` is to be propagated through;
  :param substitutes: a dictionary `Node -> A`, overrides results of `r(node)`,
    None is the same as an emtpy dictionary;
  :param incoming: operator `Node -> (List[Node], B)`, returns list of incoming nodes (dependencies)
    and some intermediate results (e.g. cached results),
    if None --- defaults to `lambda node: (get_incoming(node), None)`;
  :return: dictionary `Node -> A`.
  """
  if incoming is None:
    incoming = lambda node: (get_incoming(node), None)

  known_results = dict() if substitutes is None else dict(substitutes.items())
  graph = dict()
  intermediate_result = dict()

  stack = list()
  stack.extend(nodes)

  while len(stack) > 0:
    current_node = stack.pop()

    if current_node in known_results:
      continue

    if current_node not in graph:
      graph[current_node], intermediate_result[current_node] = incoming(current_node)

    incoming_nodes, intermediate = graph[current_node], intermediate_result[current_node]

    unknown_dependencies = [
      nodes
      for nodes in incoming_nodes
      if nodes not in known_results
    ]

    if len(unknown_dependencies) == 0:
      args = tuple(known_results[node] for node in incoming_nodes)
      known_results[current_node] = f(current_node, args, intermediate)

    else:
      intermediate_result[current_node] = intermediate
      stack.append(current_node)
      stack.extend(unknown_dependencies)

  return known_results

def reduce_graph(f, nodes, substitutes=None, incoming=None):
  """
    The same as `reduce_graph` but for `dynanic_propagate`

        r(node) = f(node, [r(x) for x in incoming(node)])

    :param f: operator to propagate, a function of type `(Node, List[A], B) -> A`;
    :param nodes: a list of nodes, output nodes of the lang `f` is to be propagated through;
    :param substitutes: a dictionary `Node -> A`, overrides results of `r(node)`,
      None is the same as an emtpy dictionary;
    :param incoming: operator `Node -> (List[Node], B)`, returns list of incoming nodes (dependencies)
      and some intermediate results (e.g. cached results),
      if None --- defaults to `lambda node: (get_incoming(node), None)`;
    :return: list with values of `r(node)` for each `node` in `nodes`,
      or just `r(node)` in case if `nodes` is a single node.
    """

  if isinstance(nodes, (tuple, list)):
    result = propagate(f, nodes, substitutes=substitutes, incoming=incoming)
    return tuple(
      result[node]
      for node in nodes
    )

  else:
    result = propagate(f, (nodes, ), substitutes=substitutes, incoming=incoming)
    return result[nodes]