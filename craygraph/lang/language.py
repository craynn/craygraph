from collections import namedtuple
from ..meta import NodeModel

from .achain import achain as achain_op
from .selector import SelectStatement, NothingStatement

__all__ = [
  'graph_language'
]

GraphLanguage = namedtuple(
  typename='GraphLanguage',
  field_names=[
    'achain',
    'repeat',
    'for_each',
    'with_inputs',
    'select',
    'seek',
    'nothing'
  ]
)

def graph_language(apply):
  class achain(NodeModel):
    def __init__(self, *definition):
      self.definition = definition

    def __call__(self, *incoming):
      return achain_op(incoming, self.definition, apply=apply)

  class _repeat(NodeModel):
    def __init__(self, n, *definition):
      self.n = n
      self.definition = definition

    def __call__(self, *incoming):
      return achain(self.definition * self.n)(*incoming)

  def repeat(n):
    def f(*definition):
      return _repeat(n, *definition)
    return f

  class for_each(NodeModel):
    def __init__(self, *definition):
      self.definition = definition

    def __call__(self, *incoming):
      return [
        achain(self.definition)(node)
        for node in incoming
      ]

  nothing = NothingStatement()

  select = SelectStatement(
    achain=achain,
    search_subgraph=False,
    replace=False
  )

  seek = SelectStatement(
    achain=achain,
    search_subgraph=True,
    replace=False
  )

  with_inputs = SelectStatement(
    achain=achain,
    search_subgraph=False,
    replace=True
  )

  return GraphLanguage(
    achain=achain,
    repeat=repeat,
    for_each=for_each,
    with_inputs=with_inputs,
    select=select,
    seek=seek,
    nothing=nothing
  )