import pytest


@pytest.fixture()
def simple_language():
  from craygraph import graph_language

  def apply(f, *incoming):
    return f(*incoming)

  return graph_language(apply)