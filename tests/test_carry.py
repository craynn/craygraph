from craygraph import carry, model_from

def test_carry():
  def f(a, x, y, *args, z=2, t=4, **kwargs):
    print(dict(a=a, x=x, y=y, args=args, z=z, t=t, kwargs=kwargs))
    return a + x + y + sum(args) + z + t + sum(kwargs.values())


  g = carry(f, fixed=dict(x=1), defaults=dict(t=2), carried=['y'])
  # a=11, x=1, y=3, args=(21, 31), z=2, t=3
  assert g(11, 21, 31, t=3)(3) == 72
  assert g(11, 21, 31, t=3)(y=3) == 72

  g = carry(f, fixed=dict(x=1), defaults=dict(t=2), carried=['args'])
  # a=11, x=3, y=3, args=(21, 31, 100), z=2, t=3
  assert g(11, y=3, t=3)(21, 31, 100) == 172
  # a=1, x=1, y=2, z=3, t=4, args=(10, 100, 1000)
  assert g(1, 2, 3, 4)(10, 100, 1000) == 1121

  def combine_kwargs1(d1, d2):
    print('combine', d1, d2)
    d1.update(d2)
    return d1

  g = carry(f, fixed=dict(t=1), defaults=dict(x=1), carried=['x', 'kwargs'], duplicated=dict(kwargs=combine_kwargs1))
  # a=11, x=21, y=3, args=(), z=200, t=1, q=10
  assert g(11, y=3, z=200, q=100)(21, q=10) == 246
  assert g(11, y=3, z=200)(21, q=10) == 246
  assert g(11, y=3, z=200, q=10)(21) == 246
  assert g(11, y=3, q=10)(21) == 48

  g = carry(f, fixed=dict(t=1), defaults=dict(x=1, keyword=100), carried=['x', 'kwargs'], duplicated=dict(kwargs=combine_kwargs1))
  assert g(11, y=3, q=10)(21) == 148
  assert g(11, y=3, q=10, keyword=200)(21) == 248
  assert g(11, y=3, q=10)(21, keyword=300) == 348
  assert g(11, y=3, q=10, keyword=200)(21, keyword=400) == 448

  g = carry(f, fixed=dict(t=1), defaults=dict(x=1), carried=['x', 'kwargs'])
  # a=11, x=21, y=3, args=(), z=200, t=1, q=10
  assert g(11, y=3, z=200)(21, q=10) == 246
  assert g(11, y=3)(21, q=10) == 48

  def combine_kwargs2(d1, d2):
    d2.update(d1)
    return d2

  g = carry(f, fixed=dict(t=1), defaults=dict(x=1), carried=['x', 'kwargs'], duplicated=dict(kwargs=combine_kwargs2))
  # a=11, x=21, y=3, args=(), z=200, t=1
  assert g(11, y=3, z=200, q=100)(21, q=10) == 336
  assert g(11, y=3, z=200, q=100)(21) == 336
  assert g(11, y=3, z=200)(x=21, q=100) == 336

  def f(*incoming, kernels=1, mode=2):
    return sum(incoming) + 10 * kernels + 100 * mode

  g = carry(f, fixed=dict(kernels=2), defaults=dict(mode=3), carried=['incoming'])

  assert g(mode=3)(9) == 329
  assert g(mode=4)(9) == 429
  assert g(mode=4,)(3, 3, 3) == 429

  def f(x, *incoming, kernels=1, mode=2):
    return x, sum(incoming), kernels, mode

  g = model_from(f).with_fixed(kernels=2).with_defaults(mode=3)()

  assert g(1, mode=3)(9) == (1, 9, 2, 3)
  assert g(7, 3)(9) == (7, 9, 2, 3)
  assert g(7, mode=3)(3, 1) == (7, 4, 2, 3)

  def f(x, y):
    return x + y

  g = carry(f, fixed=dict(), defaults=dict(y=0), carried=['y', ])
  print(g(1)())

  def f(x, y=10, model=None):
    assert model is not None
    return x + y

  g = carry(f, fixed=dict(), defaults=dict(), carried=['x', ], inject_model='model')
  assert g()(1) == 11
