import random
from craygraph import *

def test_lang_simple(simple_language):
  achain, repeat, for_each, with_inputs, select, seek, nothing = simple_language

  assert achain(
    lambda x: x + 1, # 3
    [lambda x: x + 6, lambda x: 2 * x], # 9, 6
    for_each(
      lambda x: x - 1
    ), # 8, 5
    with_inputs(0)(
      lambda x: x + 1,
      lambda x: 2 * x
    ), # 18, 5
    with_inputs(1)(
      lambda x: 2 * x,
    ), # 18, 10
    [
      select(0)(nothing),
      select(1)(lambda x: x // 2),
    ], # 18, 5
    lambda x, y: (x + 2) // y
  )(2) == 4

def test_lang(simple_language):
  achain, repeat, for_each, with_inputs, select, seek, nothing = simple_language

  a_v = random.randint(2, 10000)
  a = lambda x: x + a_v
  b_v = random.randint(2, 10000)
  b = lambda x: x * b_v
  c_v = random.randint(2, 10000)
  c = lambda x: x / c_v
  d_v = random.randint(2, 10000)
  d = lambda x: x - d_v

  for _ in range(100):
    x = random.randint(1, 100000)
    assert achain(a, b, c, d)(x) == d(c(b(a(x))))

  for _ in range(100):
    x = random.randint(1, 100000)
    assert achain(a, (b, c), d)(x) == d(c(b(a(x))))

  for _ in range(100):
    x = random.randint(1, 100000)
    assert achain(a, [b, c], lambda *xs: sum(xs))(x) == sum([
      b(a(x)), c(a(x))
    ])

def test_viz(simple_language):
  from craygraph import model_from
  from craygraph.visualize import draw_to_file

  achain, repeat, for_each, with_inputs, select, seek, nothing = simple_language

  class InputNode(Node):
    def __init__(self, name=None):
      super(InputNode, self).__init__(name=name)

    def model(self):
      return self.__class__

  class Generic(Node):
    def __init__(self, *incoming, name=None):
      super(Generic, self).__init__(*incoming, name=name)

    def __call__(self, *args):
      return args

  def named_incoming(node, _):
    incoming = get_incoming(node)
    if len(incoming) > 1:
      return dict(
        (inc.name, inc)
        for inc in incoming
      )
    else:
      return incoming

  node = model_from(Generic)()
  inputs = [InputNode('x'), InputNode('y'), InputNode('z')]
  draw_to_file(
    'linear.png',
    achain(node('a'), node('b'), node('c'), node('d'))(inputs[0]),
    vertical=False,
    selector=named_incoming
  )

  draw_to_file(
    'split.png',
    achain(node('a'), [node('b'), node('c')], node('d'))(inputs[0]),
    vertical=False,
    selector=named_incoming
  )

  draw_to_file(
    'select.png',
    achain(
      select[0, -1](
        node('a'), node('b')
      ),
      node('c')
    )(*inputs),
    inputs=inputs,
    vertical=False,
    selector=named_incoming
  )

  draw_to_file(
    'with_inputs.png',
    achain(
      with_inputs[0, -1](node('a'), node('b')), node('c')
    )(*inputs),
    inputs=inputs,
    vertical=False,
    selector=named_incoming
  )

  draw_to_file(
    'with_inputs2.png',
    achain(
      with_inputs[0, -1](
        node('a'),
        [node('b'), node('c')]
      ),
      node('d')
    )(*inputs),
    inputs=inputs,
    vertical=False,
    selector=named_incoming
  )

def test_flow(simple_language):
  achain, repeat, for_each, with_inputs, select, seek, nothing = simple_language

  class Generic(Node):
    def __init__(self, *incoming, f, name=None):
      self.f = f
      super(Generic, self).__init__(*incoming, name=name)

    def __call__(self, *args):
      return self.f(*args)

  generic = model_from(Generic)()

  class Plus(Node):
    def __init__(self, incoming1, incoming2, name=None):
      super(Plus, self).__init__(incoming1, incoming2, name=name)

    def __call__(self, x, y):
      return x + y

  def plus():
    def model(x, y):
      return Plus(x, y)

    return model

  class Sum(Node):
    def __init__(self, *incoming, name=None):
      super(Sum, self).__init__(*incoming, name=name)

    def __call__(self, *args, **kwargs):
      return sum(args)

  def summ():
    def model(*incoming):
      return Sum(*incoming)

    return model

  def get_op_output(op, args):
    args = tuple(
      x for arg in args for x in (arg if isinstance(arg, tuple) else (arg, ))
    )
    return op(*args)

  def eval_expression(nodes, substitutes=None):
    if substitutes is None:
      substitutes = dict()

    return propagate(get_op_output, nodes, substitutes=substitutes)

  class Expression(object):
    def __init__(self, inputs, outputs):
      self._inputs = inputs
      self._outputs = outputs

      self._input_names = {
        node.name : node
        for node in inputs
      }

    def __call__(self, **kwargs):
      substitutes = {
        self._input_names[name] : kwargs[name]
        for name in kwargs
      }
      return tuple(
        eval_expression(self._outputs, substitutes=substitutes)
      )

    def inputs(self):
      return self._inputs

    def outputs(self):
      return self._outputs

  def expression(*inputs):
    def model(*definition):
      input_nodes = [Node(name=name) for name in inputs]
      outputs = achain(*definition)(*input_nodes)

      return Expression(input_nodes, outputs)

    return model

  expr = expression('x', 'y', 'z')(
    with_inputs(0, 'y')(
      plus(),
      generic(lambda x: x + 1, name='inc')
    ),
    with_inputs(0, 1)(
      generic(lambda x, y: x * y, name='prod'),
      summ(),
    ),
    [
      seek('x', 0)(summ(),),
      seek('z', 0)(plus(),)
    ]
  )

  from craygraph.visualize import draw_to_file

  draw_to_file('test.png', expr)

  print(expr(x=1, y=2, z=3))
  print(dict(zip(expr.inputs(), range(3))))

  results = propagate(
    f=lambda node, args: node(*args),
    nodes=expr.outputs(),
    substitutes=dict(zip(expr.inputs(), range(3)))
  )

  print(results)

  def cached_incoming(cache):
    def incoming(node):
      if node in cache:
        return tuple(), cache[node]
      else:
        return node.incoming, None

    return incoming

  def apply(node, args, cached):
    if cached is None:
      return node(*args)
    else:
      return cached

  import random

  for _ in range(100):
    cache = dict()

    for node in get_nodes(expr.outputs()):
      if random.randint(0, 1) == 0:
        cache[node] = results[node]

    dynamic_results = dynamic.propagate(
      f=apply,
      nodes=expr.outputs(),
      incoming=cached_incoming(cache),
      substitutes=dict(zip(expr.inputs(), range(3)))
    )

    for node in dynamic_results:
      assert results[node] == dynamic_results[node]

    for node in expr.outputs():
      assert results[node] == dynamic_results[node]